<?php

return [

    'templates' => [
        'basic_1n'  => [
            'attributes'    => [
                'at_feint'          => [40, 80],
                'at_tactic'         => [40, 80],
                'at_shoot9'         => [40, 80],
                'at_shoot7'         => [40, 80],
                'at_shoot6'         => [40, 80],
                'at_pass'           => [40, 80],
                'df_colocation'     => [40, 80],
                'df_intensity'      => [40, 80],
                'df_anticipation'   => [40, 80],
                'df_solid'          => [40, 80],
                'ps_speed'          => [40, 80],
                'ps_strength'       => [40, 80],
                'ps_endurance'      => [40, 80],
                'gk_colocation'     => [40, 80],
                'gk_reflexes'       => [40, 80],
                'height'            => [170, 195],
                'weight'            => [70, 110],
                'laterality'        => [20, 80],
                'position'          => 'unknown'
            ]
        ],
        'lb_1n'  => [
            'attributes'    => [
                'at_feint'          => [60, 90],
                'at_tactic'         => [50, 80],
                'at_shoot9'         => [60, 90],
                'at_shoot7'         => [40, 60],
                'at_shoot6'         => [40, 80],
                'at_pass'           => [50, 80],
                'df_colocation'     => [60, 90],
                'df_intensity'      => [40, 80],
                'df_anticipation'   => [40, 80],
                'df_solid'          => [60, 90],
                'ps_speed'          => [40, 70],
                'ps_strength'       => [60, 90],
                'ps_endurance'      => [40, 80],
                'gk_colocation'     => [5, 50],
                'gk_reflexes'       => [5, 50],
                'height'            => [175, 195],
                'weight'            => [80, 110],
                'laterality'        => [0, 100],
                'position'          => 'lb'
            ]
        ],
        'rb_1n'  => [
            'attributes'    => [
                'at_feint'          => [60, 90],
                'at_tactic'         => [50, 80],
                'at_shoot9'         => [60, 90],
                'at_shoot7'         => [40, 60],
                'at_shoot6'         => [40, 80],
                'at_pass'           => [50, 80],
                'df_colocation'     => [60, 90],
                'df_intensity'      => [40, 80],
                'df_anticipation'   => [40, 80],
                'df_solid'          => [60, 90],
                'ps_speed'          => [40, 70],
                'ps_strength'       => [60, 90],
                'ps_endurance'      => [40, 80],
                'gk_colocation'     => [5, 50],
                'gk_reflexes'       => [5, 50],
                'height'            => [175, 195],
                'weight'            => [80, 110],
                'laterality'        => [70, 30],
                'position'          => 'rb'
            ]
        ],
        'cb_1n'  => [
            'attributes'    => [
                'at_feint'          => [70, 90],
                'at_tactic'         => [60, 90],
                'at_shoot9'         => [60, 80],
                'at_shoot7'         => [50, 80],
                'at_shoot6'         => [50, 80],
                'at_pass'           => [60, 90],
                'df_colocation'     => [60, 80],
                'df_intensity'      => [40, 80],
                'df_anticipation'   => [50, 90],
                'df_solid'          => [50, 80],
                'ps_speed'          => [50, 80],
                'ps_strength'       => [50, 80],
                'ps_endurance'      => [40, 80],
                'gk_colocation'     => [5, 50],
                'gk_reflexes'       => [5, 50],
                'height'            => [175, 185],
                'weight'            => [80, 100],
                'laterality'        => [1, 99],
                'position'          => 'cb'
            ]
        ],
        'lw_1n'  => [
            'attributes'    => [
                'at_feint'          => [50, 70],
                'at_tactic'         => [40, 70],
                'at_shoot9'         => [40, 70],
                'at_shoot7'         => [50, 90],
                'at_shoot6'         => [60, 90],
                'at_pass'           => [40, 70],
                'df_colocation'     => [40, 70],
                'df_intensity'      => [50, 90],
                'df_anticipation'   => [60, 90],
                'df_solid'          => [30, 70],
                'ps_speed'          => [60, 90],
                'ps_strength'       => [30, 70],
                'ps_endurance'      => [40, 80],
                'gk_colocation'     => [5, 50],
                'gk_reflexes'       => [5, 50],
                'height'            => [170, 180],
                'weight'            => [70, 80],
                'laterality'        => [0, 100],
                'position'          => 'lw'
            ]
        ],
        'rw_1n'  => [
            'attributes'    => [
                'at_feint'          => [50, 70],
                'at_tactic'         => [40, 70],
                'at_shoot9'         => [40, 70],
                'at_shoot7'         => [50, 90],
                'at_shoot6'         => [60, 90],
                'at_pass'           => [40, 70],
                'df_colocation'     => [40, 70],
                'df_intensity'      => [50, 90],
                'df_anticipation'   => [60, 90],
                'df_solid'          => [30, 70],
                'ps_speed'          => [60, 90],
                'ps_strength'       => [30, 70],
                'ps_endurance'      => [40, 80],
                'gk_colocation'     => [5, 50],
                'gk_reflexes'       => [5, 50],
                'height'            => [170, 180],
                'weight'            => [70, 80],
                'laterality'        => [85, 15],
                'position'          => 'rw'
            ]
        ],
        'pv_1n'  => [
            'attributes'    => [
                'at_feint'          => [30, 60],
                'at_tactic'         => [50, 80],
                'at_shoot9'         => [30, 50],
                'at_shoot7'         => [30, 70],
                'at_shoot6'         => [60, 90],
                'at_pass'           => [30, 70],
                'df_colocation'     => [60, 80],
                'df_intensity'      => [40, 80],
                'df_anticipation'   => [40, 70],
                'df_solid'          => [70, 90],
                'ps_speed'          => [40, 80],
                'ps_strength'       => [60, 90],
                'ps_endurance'      => [40, 70],
                'gk_colocation'     => [5, 50],
                'gk_reflexes'       => [5, 50],
                'height'            => [180, 195],
                'weight'            => [85, 110],
                'laterality'        => [95, 5],
                'position'          => 'pv'
            ]
        ],
        'gk_1n'  => [
            'attributes'    => [
                'at_feint'          => [0, 60],
                'at_tactic'         => [0, 60],
                'at_shoot9'         => [0, 60],
                'at_shoot7'         => [0, 60],
                'at_shoot6'         => [0, 60],
                'at_pass'           => [0, 60],
                'df_colocation'     => [0, 60],
                'df_intensity'      => [0, 60],
                'df_anticipation'   => [0, 60],
                'df_solid'          => [0, 60],
                'ps_speed'          => [40, 80],
                'ps_strength'       => [30, 70],
                'ps_endurance'      => [40, 70],
                'gk_colocation'     => [60, 90],
                'gk_reflexes'       => [60, 90],
                'height'            => [175, 195],
                'weight'            => [75, 90],
                'laterality'        => [75, 25],
                'position'          => 'gk'
            ]
        ],
        'df_1n'  => [
            'attributes'    => [
                'at_feint'          => [40, 80],
                'at_tactic'         => [40, 80],
                'at_shoot9'         => [40, 80],
                'at_shoot7'         => [40, 80],
                'at_shoot6'         => [40, 80],
                'at_pass'           => [40, 80],
                'df_colocation'     => [50, 90],
                'df_intensity'      => [50, 90],
                'df_anticipation'   => [50, 90],
                'df_solid'          => [40, 80],
                'ps_speed'          => [40, 80],
                'ps_strength'       => [40, 80],
                'ps_endurance'      => [40, 80],
                'gk_colocation'     => [40, 80],
                'gk_reflexes'       => [40, 80],
                'height'            => [180, 195],
                'weight'            => [80, 110],
                'laterality'        => [20, 80],
                'position'          => 'df'
            ]
        ],
    ]

];
