<?php

return [
    'templates' => [
        3 => [ //Primeira autonómica
            'potentials' => [
                'regular'   => [50,80],
                'special'   => [60,100],
            ],
            'players'   => [
                [
                    'probability'   => 10,
                    'template'      => 'lb_1n',
                ],
                [
                    'probability'   => 10,
                    'template'      => 'rb_1n',
                ],
                [
                    'probability'   => 10,
                    'template'      => 'cb_1n',
                ],
                [
                    'probability'   => 10,
                    'template'      => 'rw_1n',
                ],
                [
                    'probability'   => 10,
                    'template'      => 'lw_1n',
                ],
                [
                    'probability'   => 10,
                    'template'      => 'pv_1n',
                ],
                [
                    'probability'   => 5,
                    'template'      => 'gk_1n',
                ],
                [
                    'probability'   => 5,
                    'template'      => 'df_1n',
                ],
                [
                    'probability'   => 30,
                    'template'      => 'basic_1n',
                ]
            ]
        ],
    ]
];
