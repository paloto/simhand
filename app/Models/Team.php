<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

    protected $table = 'teams';

    protected $fillable = [
        'name',
        'category_id',
        'season_id',
        'club_id',
    ];

    public function club()
    {
        return $this->hasOne(Club::class, 'id', 'club_id');
    }

    public function players()
    {
        return $this->belongsToMany(Player::class, 'player_licenses', 'team_id', 'player_id');
    }

    public function phases_seasons()
    {
        return $this->belongsToMany(PhaseSeason::class, 'phase_teams', 'team_id', 'phase_season_id');
    }


}
