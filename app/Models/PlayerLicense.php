<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlayerLicense extends Model
{
    use HasFactory;

    protected $table = 'player_licenses';

    protected $fillable = [
        'season_id',
        'player_id',
        'team_id',
        'status',
    ];
}
