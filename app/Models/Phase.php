<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Phase extends Model
{
    use HasFactory;

    protected $table = 'phases';

    public function current()
    {
        $season = Season::current();
        return $this->hasMany(PhaseSeason::class, 'phase_id', 'id')->where('season_id', $season->id)->first();
    }

    public function competition()
    {
        return $this->hasOne(Competition::class, 'id', 'competition_id');
    }

}
