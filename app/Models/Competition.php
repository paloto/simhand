<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Competition extends Model
{
    use HasFactory;

    protected $table = 'competitions';

    public function phases()
    {
        return $this->hasMany(Phase::class, 'competition_id', 'id');
    }

    public function firstPhase()
    {
        return $this->hasOne(Phase::class, 'competition_id', 'id')->where('is_first', 1)->first();
    }

}
