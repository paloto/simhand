<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    use HasFactory;

    protected $table = 'match';

    protected $fillable = [
        'home_team_id',
        'away_team_id',
        'datetime',
        'home_goals',
        'away_goals',
        'round',
        'phase_season_id',
        'status',
    ];

    public function phase_season()
    {
        return $this->hasOne(PhaseSeason::class, 'id', 'phase_season_id');
    }

    public function home_team()
    {
        return $this->hasOne(Team::class, 'id', 'home_team_id');
    }

    public function away_team()
    {
        return $this->hasOne(Team::class, 'id', 'away_team_id');
    }

}
