<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhaseSeason extends Model
{
    use HasFactory;

    protected $table = 'phase_seasons';

    public function teams()
    {
        return $this->belongsToMany(Team::class, 'phase_teams', 'phase_season_id', 'team_id');
    }

    public function phase()
    {
        return $this->hasOne(Phase::class, 'id', 'phase_id');
    }

    public function season()
    {
        return $this->hasOne(Season::class, 'id', 'season_id');
    }

    public function matches()
    {
        return $this->hasMany(Match::class, 'phase_season_id', 'id');
    }

}
