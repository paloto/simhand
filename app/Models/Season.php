<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    use HasFactory;

    protected $table = 'seasons';

    public static function current()
    {
        return Season::query()
            ->where('status', 1)
            ->orderByDesc('created_at')
            ->first();
    }

}
