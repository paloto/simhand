<?php

namespace App\Models;

use App\Http\Resources\TeamResource;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class MatchEngine extends Model
{
    use HasFactory;

    public $match;
    public $path;
    public $processed_minute;

    public function __construct($match_id = null)
    {
        if(!is_null($match_id))
            $this->loadFromDB($match_id);
    }

    public function loadFromDB($match_id)
    {
        $this->match    = Match::findOrFail($match_id);
        $this->path     = 'matches/'.$this->match->phase_season->season->name.'/'.$this->match->phase_season->phase->competition->slug.'/round'.$this->match->round.'/'.$this->match->id.'.match';
    }

    public function loadFromFile()
    {
        $json = Storage::disk('local')->get($this->path);
        $this->json2object($json);
    }

    protected function json2object($json)
    {
        $array = json_decode($json);

        $match              = new Match();
        $match->id          = $array->id;
        $match->datetime    = $array->datetime;
        $match->round       = $array->round;
        $match->home_goals  = $array->home_goals;
        $match->away_goals  = $array->away_goals;
        $match->home_team   = $array->home_team;
        $match->away_team   = $array->away_team;

        $this->match = $match;

        $this->processed_minute = $array->processed_minute;

    }

    public function saveToFile()
    {

        $content = json_encode($this->matchInfo());

        Storage::disk('local')->put($this->path, $content);
    }

    public function matchInfo()
    {
        if(!isset($this->match->home_team->players)){
            $this->match->home_team->loadMissing([
                'players',
                'players.player_attributes',
            ]);
            $home_team = TeamResource::make( $this->match->home_team );
        }
        else{
            $home_team = $this->match->home_team;
        }

        if(!isset($this->match->away_team->players)) {
            $this->match->away_team->loadMissing([
                'players',
                'players.player_attributes',
            ]);
            $away_team = TeamResource::make( $this->match->away_team );
        }
        else{
            $away_team = $this->match->away_team;
        }

        return [
            'id'                => $this->match->id,
            'datetime'          => $this->match->datetime,
            'processed_minute'  => $this->processed_minute,
            'round'             => $this->match->round,
            'home_goals'        => $this->match->home_goals,
            'away_goals'        => $this->match->away_goals,
            'home_team'         => $home_team,
            'away_team'         => $away_team,
        ];
    }

}
