<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    protected $table = 'locations';

    public function getRandomNames($count = 1)
    {
        $names = Name::query()
            ->where('location_id', $this->id)
            ->where('type', 0)
            ->limit($count)
            ->inRandomOrder()
            ->pluck('name')
            ->toArray();

        $surnames =Name::query()
            ->where('location_id', $this->id)
            ->where('type', 1)
            ->limit($count)
            ->inRandomOrder()
            ->pluck('name')
            ->toArray();

        $merge_array = [];

        foreach($names as $key => $name){
            $merge_array[] = [
                $name,
                $surnames[$key]
            ];
        }

        return $merge_array;
    }

    public function getCountry()
    {
        $location = $this;
        while($location->level != 'country' && !is_null($location->level)){
            $location = Location::find($location->parent_id);
        }

        if($location->level == 'country'){
            return $location;
        }
        else{
            return null;
        }

    }

}
