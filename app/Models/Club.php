<?php

namespace App\Models;

use App\Repositories\PlayerRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    use HasFactory;

    protected $table = 'clubs';

    protected $fillable = [
        'name',
        'slug',
        'location_id',
        'universe_id',
        'emblem',
        'primary_color',
        'secondary_color',
    ];

    public function teams()
    {
        return $this->hasMany(Team::class, 'club_id', 'id')->orderBy('category_id');
    }

    public function createTeam(Category $category, Competition $competition)
    {

        $team = $this->teams()->create([
            'name'          => $this->name,
            'category_id'   => $category->id,
            'season_id'     => Season::current()->id,
        ]);

        $location = Location::find(2);

    }

    public function location()
    {
        return $this->hasOne(Location::class, 'id', 'location_id');
    }

}
