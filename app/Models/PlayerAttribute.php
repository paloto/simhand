<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlayerAttribute extends Model
{
    use HasFactory;

    const LATERALITIES = [
        0 => 'right',
        1 => 'left'
    ];

    protected $table = 'player_attributes';
    public $timestamps = false;

    protected $fillable = [
        'player_id',
        'at_feint',
        'at_tactic',
        'at_shoot9',
        'at_shoot7',
        'at_shoot6',
        'at_pass',
        'df_colocation',
        'df_intensity',
        'df_anticipation',
        'df_solid',
        'ps_speed',
        'ps_strength',
        'ps_endurance',
        'gk_colocation',
        'gk_reflexes',
        'height',
        'weight',
        'laterality',
        'potential',
    ];

    public function getLateralityAttribute()
    {
        return self::LATERALITIES[ $this->attributes['laterality']];
    }

    public function setLateralityAttribute($value)
    {
        $this->attributes['laterality'] = array_search($value, self::LATERALITIES);
    }

}
