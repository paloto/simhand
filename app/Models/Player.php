<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    use HasFactory;

    protected $table = 'players';

    protected $fillable = [
        'name',
        'birth_location',
        'birth_day',
        'career_status',
    ];

    public function player_attributes()
    {
        return $this->hasOne(PlayerAttribute::class, 'player_id', 'id');
    }

    public function positions()
    {
        return $this->hasMany(PlayerPosition::class, 'player_id', 'id')->orderByDesc('suitability');
    }

    public function teams()
    {
        return $this->hasMany(PlayerLicense::class, 'player_id', 'id');
    }

    public function calculatePosition()
    {

        $attributes         = $this->player_attributes->toArray();
        $numeric_attributes = $attributes;

        unset($numeric_attributes['player_id']);
        unset($numeric_attributes['id']);
        unset($numeric_attributes['height']);
        unset($numeric_attributes['weight']);
        unset($numeric_attributes['laterality']);

        asort($numeric_attributes, SORT_NUMERIC);

        $bigger_attribute = null;
        foreach($numeric_attributes as $key => $numeric_attribute){
            if($key != 'at_shoot7' && $key != 'ps_strength' && $key != 'at_shoot6' && $key != 'ps_endurance')
            $bigger_attribute = $key;
        }

        $position = '';
        switch($bigger_attribute
        ){
            case 'gk_colocation':
            case 'gk_reflexes':
                $position = 'gk';
                break;
            case 'ps_speed':
                if($attributes['laterality'] == 'left')
                    $position = 'rw';
                else
                    $position = 'lw';
                break;
            case 'df_colocation':
            case 'df_intensity':
            case 'df_solid':
            case 'df_anticipation':
                $position = 'df';
                break;
            case 'at_pass':
            case 'at_tactic':
                $position = 'cb';
                break;
            case 'at_shoot9':
            case 'at_feint':
                if($attributes['laterality'] == 'left')
                    $position = 'rb';
                else
                    $position = 'lb';
                break;
            default:
                $positions = ['gk', 'lb', 'lw', 'cb', 'pv', 'rb', 'rw', 'df'];
                $position = $positions[rand(0,7)];
        }

        return $position;

    }

}
