<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClubResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'          => $this->name,
            'slug'          => $this->slug,
            'location_id'   => $this->location_id,
            'universe_id'   => $this->universe_id,
            'club'          => ClubResource::make( $this->whenLoaded('club') );
        ];
    }
}
