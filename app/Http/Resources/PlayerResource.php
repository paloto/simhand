<?php

namespace App\Http\Resources;

use App\Models\PlayerAttribute;
use Illuminate\Http\Resources\Json\JsonResource;

class PlayerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'birth_location'    => $this->birth_location,
            'birth_day'         => $this->birth_day,
            'career_status'     => $this->career_status,
            'attributes'        => PlayerAttributeResource::make( $this->whenLoaded( 'player_attributes' ) ),
        ];
    }
}
