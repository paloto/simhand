<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MatchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'datetime'          => $this->datetime,
            'round'             => $this->round,
            'home_team_id'      => $this->home_team_id,
            'away_team_id'      => $this->away_team_id,
            //'home_team'         => $this->home_team,
            //'away_team'         => $this->away_team,
            'home_goals'        => $this->home_goals,
            'away_goals'        => $this->away_goals,
            'phase_season_id'   => $this->phase_season_id,
            //'phase_season'      => $this->whenLoaded() $this->phase_season,
            'status'            => $this->status,
        ];
    }
}
