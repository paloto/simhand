<?php

namespace App\Http\Controllers;

use App\Repositories\ClubRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class UsersController extends Controller
{

    public function MyTeam()
    {
        $user = Auth::user();
        $club = $user->my_club()->first();
        $team = $club->teams()->first();

        $players = $team->players()->get();

        $attribute_fields = [
            'at_feint',
            'at_tactic',
            'at_shoot9',
            'at_shoot7',
            'at_shoot6',
            'at_pass',
            'df_colocation',
            'df_intensity',
            'df_anticipation',
            'df_solid',
            'ps_speed',
            'ps_strength',
            'ps_endurance',
            'gk_colocation',
            'gk_reflexes',
            'height',
            'weight',
            'laterality',
            'potential',
        ];

        /*echo '<table><tr><td>Name</td><td>POS</td>';

        foreach($attribute_fields as $attribute_field){
            echo '<td>'.$attribute_field.'</td>';
        }
        echo '</tr>';

        foreach($players as $player){
            echo '<tr><td>'.$player->name.'</td><td>'.implode(',', $player->positions->pluck('position')->toArray()).'</td>';

            $attributes = $player->player_attributes()->first()->toArray();

            foreach($attributes as $key => $attribute){
                if($key != 'id' && $key != 'player_id'){
                    echo '<td>'.$attribute.'</td>';
                }
            }

            echo '</tr>';

        }

        echo '</table>';*/

        $emblem_url = ClubRepository::createEmblem($club);

        return view('my_team', [
            'club'              => $club,
            'team'              => $team,
            'players'           => $players,
            'attribute_fields'  => $attribute_fields,
        ]);

    }

    public function MyCompetition()
    {
        $user = Auth::user();
        $club = $user->my_club()->first();
        $team = $club->teams()->first();
        $phases_season = $team->phases_seasons->first();
        $phase = $phases_season->phase;
        $competition = $phase->competition;

        $competition->loadMissing('phases');

        return view('competition', [
            'competition'       => $competition,
            'team'              => $team,
            'matches'           => $phases_season->matches,
        ]);

    }
}
