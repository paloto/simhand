<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeamsController extends Controller
{
    public function index()
    {

        $teams = Team::all();

        echo '<h1>Teams</h1>';
        echo '<ul>';
        foreach($teams as $team){
            echo '<li><a href="teams/'.$team->id.'">'.$team->name.'</a></li>';
        }
        echo '</ul>';

    }

    public function show(Team $team)
    {

        $players = $team->players()->get();

        $attribute_fields = [
            'at_feint',
            'at_tactic',
            'at_shoot9',
            'at_shoot7',
            'at_shoot6',
            'at_pass',
            'df_colocation',
            'df_intensity',
            'df_anticipation',
            'df_solid',
            'ps_speed',
            'ps_strength',
            'ps_endurance',
            'gk_colocation',
            'gk_reflexes',
            'height',
            'weight',
            'laterality',
            'potential',
        ];

        echo '<table><tr><td>Name</td><td>POS</td>';

        foreach($attribute_fields as $attribute_field){
            echo '<td>'.$attribute_field.'</td>';
        }
        echo '</tr>';

        foreach($players as $player){
            echo '<tr><td>'.$player->name.'</td><td>'.implode(',', $player->positions->pluck('position')->toArray()).'</td>';

            //dd($player->player_attributes());

            if($player->player_attributes()){
                $attributes = $player->player_attributes()->first()->toArray();

                foreach($attributes as $key => $attribute){
                    if($key != 'id' && $key != 'player_id'){
                        echo '<td>'.$attribute.'</td>';
                    }
                }
            }
            else{
                die('ERROR PLAYER:'.$player->id);
            }

            echo '</tr>';

        }

        echo '</table>';
    }
}
