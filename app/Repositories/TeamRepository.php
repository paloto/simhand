<?php

namespace App\Repositories;

use App\Models\Club;
use App\Models\Competition;
use App\Models\Location;
use App\Models\Player;
use App\Models\PlayerAttribute;
use App\Models\PlayerLicense;
use App\Models\PlayerPosition;
use App\Models\Season;
use App\Models\Team;
use Carbon\Carbon;

class TeamRepository{

    public static function createTeam(Club $club, Competition $competition, Location $location)
    {



        $names = $location->getRandomNames(16);



    }

    public static function createPlayers(Team $team, Competition $competition)
    {

        $club       = $team->club;
        $location   = $club->location;
        $country    = $location->getCountry();

        for($i=0; $i<16; $i++){

            $name = $country->getRandomNames();

            $player = Player::create([
                'name'              => $name[0][0].' '.$name[0][1],
                'birth_location'    => 2,
                'birth_day'         => Carbon::now()->subYears(rand(16,38))->subDays(rand(0,365)),
                'career_status'     => 1,
            ]);

            PlayerLicense::create([
                'season_id' => Season::current()->id,
                'player_id' => $player->id,
                'team_id'   => $team->id,
                'status'    => 1,
            ]);

            $attributes = PlayerRepository::getAttributesFromTemplate($competition);


            PlayerAttribute::create([
                'player_id'     => $player->id,
                'at_feint'      => $attributes['at_feint'],
                'at_tactic'     => $attributes['at_tactic'],
                'at_shoot9'     => $attributes['at_shoot9'],
                'at_shoot7'     => $attributes['at_shoot7'],
                'at_shoot6'     => $attributes['at_shoot6'],
                'at_pass'       => $attributes['at_pass'],
                'df_colocation' => $attributes['df_colocation'],
                'df_intensity'  => $attributes['df_intensity'],
                'df_anticipation' => $attributes['df_anticipation'],
                'df_solid'      => $attributes['df_solid'],
                'ps_speed'      => $attributes['ps_speed'],
                'ps_strength'   => $attributes['ps_strength'],
                'ps_endurance'  => $attributes['ps_endurance'],
                'gk_colocation' => $attributes['gk_colocation'],
                'gk_reflexes'   => $attributes['gk_reflexes'],
                'height'        => $attributes['height'],
                'weight'        => $attributes['weight'],
                'laterality'    => $attributes['laterality'],
                'potential'     => $attributes['potential'],
            ]);

            if($attributes['position'] != 'unknown') {
                PlayerPosition::create([
                    'player_id'     => $player->id,
                    'position'      => $attributes['position'],
                    'suitability'   => 100,
                ]);
            }
            else{
                PlayerPosition::create([
                    'player_id'     => $player->id,
                    'position'      => $player->calculatePosition(),
                    'suitability'   => 100,
                ]);
            }

        }

    }

}
