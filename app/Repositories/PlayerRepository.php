<?php

namespace App\Repositories;

use App\Models\Competition;
use Faker\Generator as Faker;

class PlayerRepository{

    public static function getAttributesFromTemplate(Competition $competition)
    {

        $team_templates         = config('teams.templates');
        $team_potentials        = $team_templates[$competition->id]['potentials'];
        $team_template_players  = $team_templates[$competition->id]['players'];

        $probability_array = [];

        foreach($team_template_players as $key => $value){
            $probability_array[$key] = $value['probability'];
        }

        $random = rand(0,array_sum($probability_array));
        $sum    = 0;
        $key_found = null;

        foreach($probability_array as $key => $value){
            if($random < $sum + $value && is_null($key_found)){
                //BINGO
                $key_found = $team_template_players[$key]['template'];
            }
            else{
                $sum += $value;
            }
        }

        if(is_null($key_found))
            $key_found = 'basic_1n';

        $player_template = config('players.templates.'.$key_found.'.attributes');

        if(rand(0,10) == 5){
            $potential = rand($team_potentials['special'][0], $team_potentials['special'][1]);
        }
        else{
            $potential = rand($team_potentials['regular'][0], $team_potentials['regular'][1]);
        }

        $attributes = [
            'template_key'  => $key_found,
            'potential'     => $potential,
        ];

        if(is_array($player_template)){

            foreach($player_template as $key => $value){

                switch($key) {
                    case 'laterality':
                        if(rand(0,100) < $value[0])
                            $attributes[$key] = 'left';
                        else
                            $attributes[$key] = 'right';
                        break;
                    case 'position':
                        $attributes[$key] = $value;
                        break;
                    default:
                        $attributes[$key] = rand($value[0], $value[1]);
                }
            }

            return $attributes;

        }
        else{
            var_dump($player_template);
            die('Error: player:template is not an array (key_found='.$key_found.').');
        }

    }

}
