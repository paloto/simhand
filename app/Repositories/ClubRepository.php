<?php

namespace App\Repositories;

use App\Models\Club;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ClubRepository{

    public static function createEmblem(Club $club)
    {

        $path = 'emblem/'.$club->slug.'_'.uniqid().'.png';

        $colors = [
            '8a84e2',
            '604d53',
            'db7f8e',
            '9d0208',
            '023e8a',
            '03045e',
            '457b9d',
            '2a9d8f',
            '14213d',
            'fca311',
            'f77f00',
            '283618',
            'ffd60a',
            '38b000',
            '90e0ef',
            '6b705c',
            'c1121f',
        ];

        $primary_color      = $colors[array_rand($colors)];
        $secondary_color    = $colors[array_rand($colors)];

        $img = Image::canvas(600, 600);

        if(rand(1,3) == 1){
            $img->rectangle(100, 100, 500, 500, function($draw) use($colors, $primary_color){
                $draw->background($primary_color);
            });
        }
        else{
            $img->circle(400, 300, 300, function($draw) use($colors, $secondary_color){
                $draw->background($secondary_color);
            });
        }

        if(rand(1,3) == 1) {
            $img->rectangle(150, 150, 450, 450, function($draw) use($colors, $primary_color){
                $draw->background($primary_color);
            });
        }
        else{
            $img->circle(300, 300, 300, function($draw) use($colors, $secondary_color){
                $draw->background($secondary_color);
            });
        }

        $img->text( strtoupper( $club->name), 300, 300, function($font){
            $font->file('fonts/Raleway-Black.ttf');
            $font->size(30);
            $font->color('FFFFFF');
            $font->align('center');
        });

        $img->save($path);

        return [
            'path'              => $path,
            'primary_color'     => $primary_color,
            'secondary_color'   => $secondary_color,
        ];

    }

}
