<?php

namespace App\Console\Commands\Players;

use App\Models\Category;
use App\Models\Club;
use App\Models\Competition;
use App\Models\Location;
use App\Models\Match;
use App\Models\MatchEngine;
use App\Models\Player;
use Illuminate\Console\Command;
use App\Repositories\PlayerRepository;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'players:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $match_engine = new MatchEngine(63);
        $match_engine->loadFromFile();

        $match_engine->processed_minute++;

        $match_engine->saveToFile();

        $match_engine->loadFromFile();
        var_dump($match_engine->matchInfo());

        return 0;
    }
}
