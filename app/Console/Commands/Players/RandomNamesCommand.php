<?php

namespace App\Console\Commands\Players;

use App\Models\Location;
use Illuminate\Console\Command;

class RandomNamesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'players:random_name {location?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $location_id = $this->argument('location');

        $location = Location::findOrFail($location_id);

        $names = $location->getRandomName(10);

        foreach($names as $name){
            $this->line($name[0].' '.$name[1]);
        }
    }
}
