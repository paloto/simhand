<?php

namespace App\Console\Commands\Competitions;

use App\Models\Competition;
use App\Models\Season;
use App\Models\Team;
use Illuminate\Console\Command;

class EnrolCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'competitions:enrol {competition?} {team?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $competition_id = $this->argument('competition');
        $team_id        = $this->argument('team');

        $season         = Season::current();

        $competition    = Competition::findOrFail($competition_id);
        $phase          = $competition->firstPhase();

        $phase_season   = $phase->current();

        $phase_season->teams()->attach($team_id);




        return 0;
    }
}
