<?php

namespace App\Console\Commands\Competitions;

use App\Models\Match;
use App\Models\Phase;
use App\Models\Team;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CreateCalendarCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'competitions:create_calendar {phase?} {datetime_start?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $phase_id       = $this->argument('phase');
        $datetime_start = $this->argument('datetime_start');
        $datetime       = Carbon::parse($datetime_start);
        $phase          = Phase::findOrFail($phase_id);
        $phase_season   = $phase->current();
        $teams          = $phase_season->teams();

        $draw_teams     = $teams->inRandomOrder()->pluck('teams.id')->toArray();

        $calendar_template = config('competitions.calendar_templates.'.$teams->count());
        $time_between_rounds = config('competitions.time_between_rounds');

        foreach($calendar_template as $round_number => $round){
            foreach($round as $match_template){
                if(!is_null($match_template[0]) && !is_null($match_template[1])) {
                    $match = Match::create([
                        'home_team_id'      => $draw_teams[$match_template[0]],
                        'away_team_id'      => $draw_teams[$match_template[1]],
                        'datetime'          => $datetime,
                        'home_goals'        => 0,
                        'away_goals'        => 0,
                        'status'            => 1,
                        'phase_season_id'   => $phase_season->id,
                        'round'             => $round_number,
                    ]);
                }
            }
            $datetime->addSeconds($time_between_rounds);
        }


        return 0;
    }
}
