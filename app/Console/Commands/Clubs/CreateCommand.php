<?php

namespace App\Console\Commands\Clubs;

use App\Models\Club;
use App\Repositories\ClubRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Str as Str;

class CreateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clubs:create {name?} {location?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $location_id    = $this->argument('location');
        $name           = $this->argument('name');
        $slug           = Str::slug($name);

        $club = Club::create([
            'name'          => $name,
            'slug'          => $slug,
            'location_id'   => $location_id,
            'universe_id'   => 1,
        ]);

        $emblem = ClubRepository::createEmblem($club);

        $club->emblem = $emblem['path'];
        $club->primary_color = $emblem['primary_color'];
        $club->secondary_color = $emblem['secondary_color'];
        $club-save();

        return 0;
    }
}
