<?php

namespace App\Console\Commands\Teams;

use App\Models\Competition;
use App\Models\Team;
use App\Repositories\TeamRepository;
use Illuminate\Console\Command;

class CreatePlayersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'teams:create_players {team?} {competition?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $team_id        = $this->argument('team');
        $competition_id = $this->argument('competition');
        $team           = Team::findOrFail($team_id);
        $competition    = Competition::findOrFail($competition_id);

        TeamRepository::createPlayers($team, $competition);

        return 0;
    }
}
