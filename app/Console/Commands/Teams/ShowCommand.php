<?php

namespace App\Console\Commands\Teams;

use App\Models\Team;
use Illuminate\Console\Command;

class ShowCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'teams:show {team?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $team_id    = $this->argument('team');
        $team       = Team::findOrFail($team_id);
        $players    = $team->players;

        $this->line('++++++++++++++++++++++++++++++++++++');
        $this->line(' '.$team->name.' ');
        $this->line('++++++++++++++++++++++++++++++++++++');
        $this->line('');
        $this->line('PLAYERS:');

        $i=1;
        foreach($players as $player){
            $position = implode(',', $player->positions()->pluck('position')->toArray());
            $this->line('#'.$player->id.'.-  '.$player->name.' ('.$position.')');
        }



        return 0;
    }
}
