<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $competition->name }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                @foreach ($competition->phases as $phase)
                                    <h2>{{ $phase->system }}</h2>
                                    <table class="table">
                                        @foreach($phase->current()->teams as $team)
                                            <tr><td>{{ $team->name }}</td></tr>
                                        @endforeach
                                    </table>
                                @endforeach
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <h2>{{ __("Partidos") }}</h2>
                                @foreach($matches as $match)
                                    {{ __("Jornada ") . $match->round}}: {{$match->home_team->name}} VS {{$match->away_team->name}}<br/>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
