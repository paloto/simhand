<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $team->name }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8">
                                <h3>{{ __('Jugadores') }}</h3>
                                <hr/>
                                <table class="table">
                                @foreach ($players as $player)
                                    <tr><td>{{ $player->name }}</td></tr>
                                @endforeach
                                </table>
                            </div>
                            <div class="col-lg-4">
                                <img src="{{ $club->emblem }}" class="emblem" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
