<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::group(['middleware' => 'auth'], function() {

    Route::group(['namespace' => 'App\Http\Controllers'], function(){

        Route::get('/dashboard', 'IndexController@index')->name('dashboard');

        Route::get('/my_team',          'UsersController@MyTeam')       ->name('my_team');
        Route::get('/my_competition',   'UsersController@MyCompetition')->name('my_competition');

        Route::group(['prefix' => 'competitions'], function(){
            Route::get('/',     'CompetitionsController@index')->name('competitions');
        });

        Route::group(['prefix' => 'teams'], function(){
            Route::get('/',         'TeamsController@index')->name('teams');
            Route::get('/{team}',   'TeamsController@show')->name('teams.show');
        });

        Route::group(['prefix' => 'players'], function(){
            Route::get('/{player}',   'PlayersController@show')->name('players.show');
        });

    });



});

require __DIR__.'/auth.php';
