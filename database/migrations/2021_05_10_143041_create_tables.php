<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('universes', function (Blueprint $table) {
            $table->id();
            $table->string(                 'name', 128)->nullable();
            $table->unsignedBigInteger(     'creator_id');
            $table->unsignedTinyInteger(    'status');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('seasons', function (Blueprint $table) {
            $table->id();
            $table->string(             'name', 16);
            $table->date(               'date_from');
            $table->date(               'date_to');
            $table->unsignedTinyInteger('status');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('locations', function (Blueprint $table) {
            $table->id();
            $table->string(             'name');
            $table->string(             'level', 32);
            $table->unsignedBigInteger( 'parent_id')->nullable();
            $table->unsignedBigInteger( 'population');
        });

        Schema::create('clubs', function (Blueprint $table) {
            $table->id();
            $table->string(             'name');
            $table->string(             'slug', 64)->nullable();
            $table->unsignedBigInteger( 'location_id');
            $table->unsignedBigInteger( 'universe_id');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string(             'name');
            $table->unsignedTinyInteger('age_since' );
            $table->unsignedTinyInteger('years');
            $table->unsignedBigInteger( 'previous_category_id')->nullable();
            $table->unsignedTinyInteger('gender')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('teams', function (Blueprint $table) {
            $table->id();
            $table->string(             'name');
            $table->unsignedBigInteger( 'category_id')->nullable();
            $table->unsignedBigInteger( 'season_id')->nullable();
            $table->unsignedBigInteger( 'club_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('federations', function (Blueprint $table) {
            $table->id();
            $table->string(             'name');
            $table->unsignedBigInteger( 'location_id');
            $table->unsignedBigInteger( 'universe_id');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('competitions', function (Blueprint $table) {
            $table->id();
            $table->string(             'name');
            $table->unsignedBigInteger( 'federation_id');
            $table->unsignedBigInteger( 'category_id');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('competition_seasons', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger( 'competition_id');
            $table->unsignedBigInteger( 'season_id');
            $table->unsignedTinyInteger('status');
        });

        Schema::create('phases', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('competition_id');
            $table->unsignedTinyInteger('participants');
            $table->string(             'system')->default('league');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('phase_seasons', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('phase_id');
            $table->unsignedBigInteger('season_id');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('phase_classifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger( 'from_phase_id');
            $table->unsignedBigInteger( 'to_phase_id');
            $table->boolean(            'next_season')->default(false);
            $table->unsignedTinyInteger('position');
        });

        Schema::create('officials', function (Blueprint $table) {
            $table->id();
            $table->string(             'name');
            $table->unsignedBigInteger( 'user_id')->nullable();
            $table->unsignedBigInteger( 'birth_location');
            $table->date(               'birth_day');
            $table->tinyInteger(        'career_status');
            $table->string(             'main_position');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('official_positions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger( 'official_id');
            $table->unsignedTinyInteger('suitability')->default(100);
        });

        Schema::create('players', function (Blueprint $table) {
            $table->id();
            $table->string(             'name');
            $table->unsignedBigInteger( 'birth_location');
            $table->date(               'birth_day');
            $table->tinyInteger(        'career_status');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('player_positions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger( 'player_id');
            $table->unsignedTinyInteger('suitability')->default(100);
        });

        Schema::create('player_licenses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger(     'season_id');
            $table->unsignedBigInteger(     'player_id');
            $table->unsignedBigInteger(     'team_id');
            $table->unsignedTinyInteger(    'status');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('official_licenses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger(     'season_id');
            $table->unsignedBigInteger(     'official_id');
            $table->unsignedBigInteger(     'team_id');
            $table->unsignedTinyInteger(    'status');
            $table->string(                 'position');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('match', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger( 'home_team_id');
            $table->unsignedBigInteger( 'away_team_id');
            $table->dateTime(           'datetime');
            $table->unsignedTinyInteger('home_goals')->default(0);
            $table->unsignedTinyInteger('away_goals')->default(0);
            $table->unsignedBigInteger( 'competition_season_id');
            $table->unsignedTinyInteger('status');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('match_players', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger( 'player_id');
            $table->unsignedBigInteger( 'match_id');
        });

        Schema::create('match_officials', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger( 'official_id');
            $table->unsignedBigInteger( 'match_id');
        });

        Schema::create('player_attributes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger( 'player_id');
            $table->unsignedTinyInteger('attack');
            $table->unsignedTinyInteger('defense');
            $table->unsignedTinyInteger('goalkeeper');
        });

        Schema::table('users', function(Blueprint $table) {
            $table->unsignedBigInteger('club_id')->nullable();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('universes');
        Schema::dropIfExists('seasons');
        Schema::dropIfExists('locations');
        Schema::dropIfExists('clubs');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('teams');
        Schema::dropIfExists('federations');
        Schema::dropIfExists('competitions');
        Schema::dropIfExists('competition_seasons');
        Schema::dropIfExists('phases');
        Schema::dropIfExists('phase_seasons');
        Schema::dropIfExists('phase_clasifications');
        Schema::dropIfExists('officials');
        Schema::dropIfExists('players');
        Schema::dropIfExists('official_positions');
        Schema::dropIfExists('player_positions');
        Schema::dropIfExists('player_licenses');
        Schema::dropIfExists('official_licenses');
        Schema::dropIfExists('match');
        Schema::dropIfExists('match_players');
        Schema::dropIfExists('match_officials');
        Schema::dropIfExists('player_attributes');

        Schema::table('users', function (Blueprint $table){
            $table->dropColumn('club_id');
        });



    }
}
