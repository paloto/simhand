<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePlayerAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('player_attributes', function (Blueprint $table){
            $table->dropColumn('attack');
            $table->dropColumn('defense');
            $table->dropColumn('goalkeeper');
        });

        Schema::table('player_attributes', function(Blueprint $table) {
            $table->unsignedTinyInteger('at_feint')->nullable();
            $table->unsignedTinyInteger('at_tactic')->nullable();
            $table->unsignedTinyInteger('at_shoot9')->nullable();
            $table->unsignedTinyInteger('at_shoot7')->nullable();
            $table->unsignedTinyInteger('at_shoot6')->nullable();
            $table->unsignedTinyInteger('at_pass')->nullable();
            $table->unsignedTinyInteger('df_colocation')->nullable();
            $table->unsignedTinyInteger('df_intensity')->nullable();
            $table->unsignedTinyInteger('df_anticipation')->nullable();
            $table->unsignedTinyInteger('df_solid')->nullable();
            $table->unsignedTinyInteger('ps_speed')->nullable();
            $table->unsignedTinyInteger('ps_strength')->nullable();
            $table->unsignedTinyInteger('ps_endurance')->nullable();
            $table->unsignedTinyInteger('gk_colocation')->nullable();
            $table->unsignedTinyInteger('gk_reflexes')->nullable();
            $table->unsignedSmallInteger('height')->nullable();
            $table->unsignedSmallInteger('weight')->nullable();
            $table->unsignedSmallInteger('laterality')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('player_attributes', function (Blueprint $table){
            $table->unsignedTinyInteger('attack');
            $table->unsignedTinyInteger('defense');
            $table->unsignedTinyInteger('goalkeeper');
        });

        Schema::table('player_attributes', function(Blueprint $table) {
            $table->dropColumn('at_feint');
            $table->dropColumn('at_tactic');
            $table->dropColumn('at_shoot9');
            $table->dropColumn('at_shoot7');
            $table->dropColumn('at_shoot6');
            $table->dropColumn('at_pass');
            $table->dropColumn('df_colocation');
            $table->dropColumn('df_intensity');
            $table->dropColumn('df_anticipation');
            $table->dropColumn('df_solid');
            $table->dropColumn('ps_speed');
            $table->dropColumn('ps_strength');
            $table->dropColumn('ps_endurance');
            $table->dropColumn('gk_colocation');
            $table->dropColumn('gk_reflexes');
            $table->dropColumn('height');
            $table->dropColumn('weight');
            $table->dropColumn('laterality');
        });
    }
}
