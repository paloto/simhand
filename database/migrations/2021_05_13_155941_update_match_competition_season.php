<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMatchCompetitionSeason extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('match', function (Blueprint $table){
            $table->dropColumn('competition_season_id');
            $table->unsignedBigInteger('phase_season_id')->after('away_goals');
            $table->unsignedTinyInteger('round')->after('away_goals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('match', function (Blueprint $table){
            $table->unsignedBigInteger('competition_season_id');
            $table->dropColumn('phase_season_id');
            $table->dropColumn('round');
        });
    }
}
