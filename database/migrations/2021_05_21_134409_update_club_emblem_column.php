<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateClubEmblemColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table){
            $table->string('emblem', 128)->after('slug');
            $table->string('primary_color', 12)->after('emblem');
            $table->string('secondary_color', 12)->after('emblem');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clubs', function (Blueprint $table){
            $table->dropColumn('emblem');
            $table->dropColumn('primary_color');
            $table->dropColumn('secondary_color');
        });
    }
}
