<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UniverseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('universes')->truncate();

        DB::table('universes')->insert([
            'id'            => 1,
            'name'          => 'simverse',
            'creator_id'    => 1,
            'status'        => 1,
        ]);
    }
}
