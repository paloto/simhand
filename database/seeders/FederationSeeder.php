<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FederationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $federations = [
            [
                'id'            => 1,
                'name'          => 'Simulated Handball Federation',
                'location_id'   => 1,
                'universe_id'   => 1,
            ],
            [
                'id'            => 2,
                'name'          => 'Irreal Federación Española de Balonmano',
                'location_id'   => 2,
                'universe_id'   => 1,
            ],
            [
                'id'            => 3,
                'name'          => 'Federación Galega de Balonmán',
                'location_id'   => 3,
                'universe_id'   => 1,
            ]
        ];

        DB::table('federations')->truncate();

        foreach($federations as $federation){
            DB::table('federations')->insert($federation);
        }

    }
}
