<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompetitionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $competitions = [
            [
                'id'            => 1,
                'name'          => 'SHF Champions League',
                'federation_id' => 1,
                'category_id'   => 1,
            ],
            [
                'id'            => 2,
                'name'          => 'ASSOBAL',
                'federation_id' => 2,
                'category_id'   => 1,
            ],
            [
                'id'            => 3,
                'name'          => 'Primeira autonómica',
                'federation_id' => 3,
                'category_id'   => 1,
            ]
        ];

        DB::table('competitions')->truncate();

        foreach($competitions as $competition){
            DB::table('competitions')->insert($competition);
        }

        $competition_seasons = [
            [
                'id'                => 1,
                'competition_id'    => 1,
                'season_id'         => 1,
                'status'            => 1,
            ],
            [
                'id'                => 2,
                'competition_id'    => 2,
                'season_id'         => 1,
                'status'            => 1,
            ],
            [
                'id'                => 3,
                'competition_id'    => 3,
                'season_id'         => 1,
                'status'            => 1,
            ],
        ];

        DB::table('competition_seasons')->truncate();

        foreach($competition_seasons as $competition_season){
            DB::table('competition_seasons')->insert($competition_season);
        }

        $phases = [
            [
                'id'                => 1,
                'competition_id'    => 1,
                'participants'      => 16,
                'system'            => 'league',
            ],
            [
                'id'                => 2,
                'competition_id'    => 2,
                'participants'      => 16,
                'system'            => 'league',
            ],
            [
                'id'                => 3,
                'competition_id'    => 3,
                'participants'      => 16,
                'system'            => 'league',
            ],
        ];

        DB::table('phases')->truncate();

        foreach($phases as $phase){
            DB::table('phases')->insert($phase);
        }

        $phase_seasons = [
            [
                'id'        => 1,
                'phase_id'  => 1,
                'season_id' => 1,
            ],
            [
                'id'        => 2,
                'phase_id'  => 2,
                'season_id' => 1,
            ],
            [
                'id'        => 3,
                'phase_id'  => 3,
                'season_id' => 1,
            ],
        ];

        DB::table('phase_seasons')->truncate();

        foreach($phase_seasons as $phase_season){
            DB::table('phase_seasons')->insert($phase_season);
        }

        $phase_clasifications = [
            [
                'id'            => 1,
                'from_phase_id' => 2,
                'to_phase_id'   => 1,
                'next_season'   => 1,
                'position'      => 1,
            ],
            [
                'id'            => 2,
                'from_phase_id' => 2,
                'to_phase_id'   => 1,
                'next_season'   => 1,
                'position'      => 2,
            ],
            [
                'id'            => 3,
                'from_phase_id' => 2,
                'to_phase_id'   => 1,
                'next_season'   => 1,
                'position'      => 3,
            ],
        ];

        DB::table('phase_clasifications')->truncate();

        foreach($phase_clasifications as $phase_clasification){
            DB::table('phase_clasifications')->insert($phase_clasification);
        }

    }
}
