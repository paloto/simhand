<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'id'            => 1,
                'name'          => 'Senior masculino',
                'age_since'     => 18,
                'years'         => 30,
                'previous_category_id'  => null,
                'gender'        => 1,
            ]
        ];

        DB::table('categories')->truncate();

        foreach($categories as $category){
            DB::table('categories')->insert($category);
        }
    }
}
