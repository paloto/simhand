<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('locations')->truncate();

        DB::table('locations')->insert([
            'id'            => 1,
            'name'          =>'Europe',
            'level'         => 'continent',
            'parent_id'     => null,
            'population'    => 0
        ]);

        DB::table('locations')->insert([
            'id'            => 2,
            'name'          =>'Spain',
            'level'         => 'country'  ,
            'parent_id'     => 1,
            'population'    => 0
        ]);

        DB::table('locations')->insert([
            'id'            => 3,
            'name'          =>'Galician',
            'level'         => 'state',
            'parent_id'     => 2,
            'population'    => 0
        ]);

        DB::table('locations')->insert([
            'id'            => 4,
            'name'          =>'Pontevedra',
            'level'         => 'province' ,
            'parent_id'     => 3,
            'population'    => 0
        ]);

        DB::table('locations')->insert([
            'id'            => 5,
            'name'          =>'Vigo',
            'level'         => 'city' ,
            'parent_id'     => 4,
            'population'    => 0
        ]);
    }
}
