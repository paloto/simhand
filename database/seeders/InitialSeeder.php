<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class InitialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $universe_seeder    = new UniverseSeeder();
        $season_seeder      = new SeasonSeeder();
        $category_seeder    = new CategorySeeder();
        $location_seeder    = new LocationSeeder();
        $federation_seeder  = new FederationSeeder();
        $club_seader        = new ClubSeeder();
        $competition_seeder = new CompetitionSeeder();

        $universe_seeder->run();
        $season_seeder->run();
        $category_seeder->run();
        $location_seeder->run();
        $federation_seeder->run();
        $club_seader->run();
        $competition_seeder->run();

    }
}
