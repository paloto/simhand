<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SeasonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seasons = [
            [
                'id'            => 1,
                'name'          => '2020-2021',
                'date_from'     => '2020-08-01',
                'date_to'       => '2021-07-31',
                'status'        => 1
            ]
        ];

        DB::table('seasons')->truncate();

        foreach($seasons as $season){
            DB::table('seasons')->insert($season);
        }
    }
}
